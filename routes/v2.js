const express = require('express');
const post = require('../controller/post');

const router = express.Router();

// /v2/hello
router.get('/hello', (req, res) => {
  res.send('hello from v2');
});

router.post('/post', post);

module.exports = router;
