const express = require('express');
const logger = require('./middleware/logger');
const v2Router = require('./routes/v2');

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(logger);

app.use(express.static('public'));

app.post('/post', (req, res) => {
  res.json({
    msg: 'This is a Post Req',
    method: req.method,
    data: req.body,
  });
});

app.get('/hello', (req, res) => {
  res.send('Hey there!');
});

// /v1/hello - GET
// /v1/post - POST

app.use('/v2/hello', (req, res) => {
  res.send('Hey there from v2!');
});

app.post('/post', (req, res) => {
  res.json({
    msg: 'This is a Post Req from v2',
    method: req.method,
    data: req.body,
  });
});

app.use('/v2', v2Router);

app.use((req, res) => res.status(404).send('404 Not found'));

app.listen(PORT, () => console.log(`server is running on port ${PORT}`));
