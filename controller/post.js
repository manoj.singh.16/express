module.exports = (req, res) => {
  res.json({
    msg: 'POST from v2',
    method: req.method,
    data: req.body,
  });
};
