const http = require('http');

const app = http.createServer((req, res) => {
  switch (req.url) {
    case '/':
      res.writeHead(300);
      res.end('This is home');
      break;
    case '/about':
      if (req.method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
          body.push(chunk);
        });
        req.on('end', () => {
          console.log(body.join(''));
        });
        res.end('This is about');
      } else {
        res.end('404');
      }
      break;
    default:
      res.end('404 not found');
  }
});

app.listen(3000, () => console.log(`Server is listening on port 3000`));
